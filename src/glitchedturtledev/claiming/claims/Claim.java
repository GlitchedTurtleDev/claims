package glitchedturtledev.claiming.claims;

import java.util.ArrayList;
import java.util.UUID;

import org.bukkit.Chunk;

public class Claim {
	private UUID owner;
	private ArrayList<Chunk> chunks;
	
	public Claim(UUID owner, ArrayList<Chunk> chunks)
	{
		this.owner = owner;
		this.chunks = chunks;
	}
	
	public void claim(Chunk c)
	{
		chunks.add(c);
	}
	
	public boolean isOwned(Chunk c)
	{
		return chunks.contains(c);
	}

	public UUID getOwner() {
		return owner;
	}

	public ArrayList<Chunk> getChunks() {
		return chunks;
	}
	
	
}
