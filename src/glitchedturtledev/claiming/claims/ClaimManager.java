package glitchedturtledev.claiming.claims;

import java.util.HashMap;
import java.util.UUID;

import org.bukkit.Chunk;

public class ClaimManager {
	private HashMap<UUID, Claim> claims = new HashMap<>();
	
	public Claim getClaim(UUID owner)
	{
		return claims.get(owner);
	}
	
	public boolean isOwned(Chunk ch)
	{
		for(Claim c : claims.values())
		{
			if(c.isOwned(ch)) return true;
		}
		return false;
	}
	
	public void register(Claim c)
	{
		claims.put(c.getOwner(), c);
	}
	
	public Claim getOwner(Chunk ch)
	{
		for(Claim c : claims.values())
		{
			if(c.isOwned(ch)) return c;
		}
		return null;
	}
	
}
