package glitchedturtledev.claiming;

import glitchedturtledev.claiming.claims.ClaimManager;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;
import org.bukkit.plugin.java.JavaPlugin;

public class Claims extends JavaPlugin implements Listener {
	private ClaimManager claimManager;
	
	public void onEnable()
	{
		//TODO: Deserialization of config.
		claimManager = new ClaimManager();
		this.getServer().getPluginManager().registerEvents(this,this);
	}
	
	public ClaimManager getClaimManager()
	{
		return claimManager;
	}
	
	
	@EventHandler
	public void onCommand(PlayerCommandPreprocessEvent event)
	{
		if(event.getMessage().equals("/claim"))
		{
			if(claimManager.getClaim(event.getPlayer().getUniqueId()) == null)
			{
				event.getPlayer().sendMessage("Claims> " + "How are you suppost to claim land without owning a kingdom? Your a fucking retard");
				return;
			}
			
			if(claimManager.isOwned(event.getPlayer().getLocation().getChunk()))
			{
				event.getPlayer().sendMessage("Claims> " + "This land is already claimed");
				return;
			}
			claimManager.getClaim(event.getPlayer().getUniqueId()).claim(event.getPlayer().getLocation().getChunk());
			event.getPlayer().sendMessage("Claims> " + "You have claimed this area.");
		}
	}
	
}
